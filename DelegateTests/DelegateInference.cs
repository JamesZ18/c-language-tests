﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DelegateTests
{
    [TestClass]
    public class DelegateInference
    {
        private int indicator = 0;
        public delegate void SimpleDelegate();

        [TestMethod]
        public void DelegateWithoutInference_WhenCalled_Works()
        {
            new SimpleDelegate(SimpleHandler)?.Invoke();
            int result = indicator;
            indicator = 0;
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void DelegateWithInference_WhenCalled_Works()
        {
            SimpleDelegate sd = SimpleHandler;
            sd();
            int result = indicator;
            indicator = 0;
            Assert.AreEqual(1, result);
        }

        private void SimpleHandler()
        {
            indicator = 1;
        }
    }
}

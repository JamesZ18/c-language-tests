﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SyntaxTests
{
    [TestClass]
    public class PrimitiveDeclarationTests
    {
        [TestMethod]
        public void IntDeclaration_WithReturnsAfterEqualSign_WorksNormally()
        {
            // Arrange + Act
            int x =












                5;
            bool xIs5 = x == 5;
            // Assert
            Assert.AreEqual(true, xIs5);
        }
    }
}

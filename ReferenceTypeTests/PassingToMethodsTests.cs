﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ReferenceTypeTests
{
    [TestClass]
    public class PassingToMethodsTests
    {
        [TestMethod]
        public void ObjectPassedToMethod_OnChangeInMethod_IsReflectedElsewhere()
        {
            // Arrange
            TestClass1 object1 = new TestClass1() { Int1 = 0 };
            // Act
            ChangeObject(object1);
            bool object1IsChanged = (object1.Int1 == 1);
            // Assert
            Assert.AreEqual(true, object1IsChanged);
        }

        public void ChangeObject(TestClass1 testObject) => testObject.Int1 = 1;

        [TestMethod]
        public void IntPassedAsRef_OnChangeInMethod_IsReflectedElsewhere()
        {
            // Arrange
            int int1 = 0;
            // Act
            ChangeIntByRef(ref int1);
            bool int1IsChanged = int1 == 1;
            // Assert
            Assert.AreEqual(true, int1IsChanged);
        }

        public void ChangeIntByRef(ref int intToChange) => intToChange = 1;

        [TestMethod]
        public void IntPassedToMethod_OnChangeInMethod_IsNotReflectedElsewhere()
        {
            // Arrange
            int int1 = 0;
            // Act
            ChangeInt(int1);
            bool int1IsChanged = int1 == 1;
            // Assert
            Assert.AreEqual(false, int1IsChanged);
        }

        public void ChangeInt(int intToChange) => intToChange = 1;
    }
}

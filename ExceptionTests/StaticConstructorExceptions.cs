﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExceptionTests
{
    [TestClass]
    public class StaticConstructorExceptions
    {
        [TestMethod]
        public void TestMethod1()
        {
            try
            {
                for (int i = 0; i < 6; i++)
                {
                    Console.WriteLine(PublicStaticConstructor.Head);
                }
            }
            catch
            {
                Console.WriteLine("Caught exception, continuing");
            }
            try
            {
                Console.WriteLine(PublicStaticConstructor.available);
            }
            catch
            {
                Console.WriteLine("Class wasn't available");
            }
            Assert.AreEqual(true, true);
        }
    }
}

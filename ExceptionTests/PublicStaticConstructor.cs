﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionTests
{
    static class PublicStaticConstructor
    {
        public static string available = "Still Available";
        private static int numLeft = 0;
        public static int Head
        {
            get
            {
                return GetNext();
            }
        }
        static PublicStaticConstructor()
        {
            Console.WriteLine("I'm only executed once.");
            numLeft = 5;
        }

        private static int GetNext()
        {
            if (numLeft > 0)
            {
                numLeft--;
                Console.WriteLine("I'm executed each time Head is called");
                return 1;
            }
            else
            {
                throw new Exception();
            }
        }
    }
}

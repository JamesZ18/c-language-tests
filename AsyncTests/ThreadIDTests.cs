﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncTests
{
    [TestClass]
    public class ThreadIDTests
    {
        private static Object lockObj = new Object();

        [TestMethod]
        public void RanTask_OnIsEqualToCallerThreadID_ISFalse()
        {
            ShowThreadInformation("Starter Thread");
            Task t = new Task(() => { ShowThreadInformation("Starter Thread Task"); });
            t.Start();
            AsyncWrapper();

            Assert.AreEqual(true, true);
        }

        private static void ShowThreadInformation(String taskName)
        {
            String msg = null;
            Thread thread = Thread.CurrentThread;
            lock (lockObj)
            {
                msg = String.Format("{0} thread information\n", taskName) +
                      String.Format("   Background: {0}\n", thread.IsBackground) +
                      String.Format("   Thread Pool: {0}\n", thread.IsThreadPoolThread) +
                      String.Format("   Thread ID: {0}\n", thread.ManagedThreadId);
            }
            Console.WriteLine(msg);
        }

        private static async void AsyncWrapper()
        {
            ShowThreadInformation("Main Thread");
            await Task.Run(() =>
            {
                ShowThreadInformation("Worker Thread");
            });

            ShowThreadInformation("Suspected Main Thread");
        }

    }
}

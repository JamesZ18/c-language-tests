﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethodTests
{
    public static class TestStringExtMethod
    {
        public static string TestExtMethod(this string source) => "Extension";
    }
}

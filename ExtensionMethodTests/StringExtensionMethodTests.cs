﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExtensionMethodTests
{
    [TestClass]
    public class StringExtensionMethodTests
    {
        // This method just ensures the syntax is correct.
        [TestMethod]
        public void StringExtMethod_OnCall_ReturnsValue()
        {
            // Arrange
            string actual = "Starting Value";
            string expected = "Extension";

            // Act
            actual = actual.TestExtMethod();

            // Assert
            Assert.AreEqual(actual, expected);
        }
    }
}

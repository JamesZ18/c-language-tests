﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClassTests
{
    [TestClass]
    public class EqualityTests
    {
        [TestMethod]
        public void ObjectsWithSamePropValues_OnDefaultDotEquals_IsFalse()
        {
            // Arrange
            TestClass1 object1 = new TestClass1() { Int1 = 0, Int2 = 1 };
            TestClass1 object2 = new TestClass1() { Int1 = 0, Int2 = 1 };
            // Act
            bool object1EqualsObject2 = object1.Equals(object2);
            // Assert
            Assert.AreEqual(false, object1EqualsObject2);
        }

        [TestMethod]
        public void ObjectsWithSamePropValues_OnDefaultEqualityOperator_IsFalse()
        {
            // Arrange
            TestClass1 object1 = new TestClass1() { Int1 = 0, Int2 = 1 };
            TestClass1 object2 = new TestClass1() { Int1 = 0, Int2 = 1 };
            // Act
            bool object1EqualsObject2 = object1 == object2;
            // Assert
            Assert.AreEqual(false, object1EqualsObject2);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructTests
{
    public struct TestStruct
    {
        public int Int1 { get; set; }
        public int Int2 { get; set; }
    }
}

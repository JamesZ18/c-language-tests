﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StructTests
{
    /// <summary>
    /// Includes:
    ///     Struct Equality
    ///         Note: Struct values cannot be automatically compared using the == operator.
    /// </summary>
    [TestClass]
    public class StructEquality
    {
        [TestMethod]
        public void StructsWithSamePropValues_OnDotEquals_IsTrue()
        {
            // Arrange
            TestStruct struct1 = new TestStruct() { Int1 = 0, Int2 = 1 };
            TestStruct struct2 = new TestStruct { Int1 = 0, Int2 = 1 };
            // Act
            bool struct1And2AreEqual = (struct1.Equals(struct2));
            // Assert
            Assert.AreEqual(true, struct1And2AreEqual);
        }

        [TestMethod]
        public void StructsWithDifferentPropValues_OnDotEquals_IsFalse()
        {
            // Arrange
            TestStruct struct1 = new TestStruct() { Int1 = 0, Int2 = 2 };
            TestStruct struct2 = new TestStruct { Int1 = 0, Int2 = 1 };
            // Act
            bool struct1And2AreEqual = (struct1.Equals(struct2));
            // Assert
            Assert.AreEqual(false, struct1And2AreEqual);
        }

        [TestMethod]
        public void StructsWithShuffledPropValues_OnDotEquals_IsFalse()
        {
            // Arrange
            TestStruct struct1 = new TestStruct() { Int1 = 1, Int2 = 0 };
            TestStruct struct2 = new TestStruct { Int1 = 0, Int2 = 1 };
            // Act
            bool struct1And2AreEqual = (struct1.Equals(struct2));
            // Assert
            Assert.AreEqual(false, struct1And2AreEqual);
        }
    }
}

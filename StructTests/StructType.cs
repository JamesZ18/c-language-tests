﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StructTests
{
    [TestClass]
    public class StructType
    {
        [TestMethod]
        public void StructPassedToMethod_OnChangeInMethod_IsNotReflectedElsewhere()
        {
            // Arrange
            TestStruct struct1 = new TestStruct() { Int1 = 0, Int2 = 1 };

            // Act
            AttemptToChangeStruct(struct1);
            bool struct1IsChanged = struct1.Int1 == 1;

            // Assert
            Assert.AreEqual(false, struct1IsChanged);
        }

        public void AttemptToChangeStruct(TestStruct structToChange) => structToChange.Int1 = 1;
    }
}

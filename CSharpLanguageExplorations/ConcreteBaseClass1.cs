﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLanguageExplorations
{
    public class ConcreteBaseClass1
    {
        public string TestString { get { return "Base1"; } }
        //can also be expressed in expression form:
        //public string TestString => "Base1";
        public virtual void Overridden_Cast_To_Base() => Console.WriteLine("BaseClass1 Overridden_Cast_To_Base");
    }
}

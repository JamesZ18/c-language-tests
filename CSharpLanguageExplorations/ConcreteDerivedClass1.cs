﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLanguageExplorations
{
    class ConcreteDerivedClass1 : ConcreteBaseClass1
    {
        public new string TestString { get { return "Derived1"; } }
        public override void Overridden_Cast_To_Base()
        {
            Console.WriteLine("DerivedClass1 Overridden_Cast_To_Base");         
        }

    }
}

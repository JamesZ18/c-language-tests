﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLanguageExplorations
{
    class Program
    {
        static void Main(string[] args)
        {        
            // Testing whether derived objects with overridden methods will use the base class's method or the overridden one.
            ConcreteBaseClass1 baseInstance = new ConcreteBaseClass1();
            ConcreteDerivedClass1 derivedInstance = new ConcreteDerivedClass1();

            Console.WriteLine("Base1 Overridden_Cast_To_Base output:");
            baseInstance.Overridden_Cast_To_Base();
            Console.WriteLine("Derived1 Overridden_Cast_To_Base output:");
            derivedInstance.Overridden_Cast_To_Base();
            Console.WriteLine("Derived1 cast to Base1 Overridden_Cast_To_Base output:");
            ((ConcreteBaseClass1)derivedInstance).Overridden_Cast_To_Base();
            // Ensuring it's seen by the compiler as BaseClass1 Type:
            ConcreteBaseClass1 derivedTest = derivedInstance as ConcreteBaseClass1;
            if (derivedTest != null)
            {
                Console.WriteLine("as statement didn't return null, ie no exception in cast");
            }
            // Result: "DerivedClass1 Overridden_Cast_To_Base", even though the as statement didn't return a null.
            // That is, a derived object will use the method in the derived class.

            // Testing whether base objects cast to derived classes with overridden methods will call the base or derived method.
            //Console.WriteLine(((DerivedClass1)baseInstance).Overridden_Cast_To_Base());
            ConcreteDerivedClass1 baseTest = baseInstance as ConcreteDerivedClass1;
            if (baseTest != null)
            {
                Console.WriteLine("as statement didn't return null, ie no exception in cast");
            }
            else
            {
                Console.WriteLine("as statement returned null, baseInstance cannot be typed as derived class");
            }
            // Result: Base class cannot be cast to instantiated class.

            // Testing whether casting a derived type to a base type and attempting to access a property will use the base type or derived type.
            Console.WriteLine($"Derived type: {derivedInstance.TestString}");
            Console.WriteLine($"Base type: {baseInstance.TestString}");
            Console.WriteLine($"Derived cast to base: {derivedTest.TestString}");
            // Result: "Derived cast to base: Base1", meaning it works.  You can have hierarchy level specific members sharing the same name.
            // This leads to another question: will the TestString for a child of Derived1 have the derived or base string by default?
            ConcreteDerivedClass1_1 CDC11Obj = new ConcreteDerivedClass1_1();
            Console.WriteLine($"CDC11 Default TestString: {CDC11Obj.TestString}");
            // Result: "Derived1".  So, as one might expect, if will refer to the default of its parent.



        }
    }
}
